//! UART driver, cc26xx family
use kernel::common::regs::{ReadOnly, ReadWrite, WriteOnly};
use kernel::hil::gpio::Pin;
use core::cell::Cell;
use kernel;

use prcm;
use cc26xx::gpio;
use ioc;

pub const UART_BASE: usize = 0x4000_1000;
pub const MCU_CLOCK: u32 = 48_000_000;

#[repr(C)]
struct Registers {
    dr: ReadWrite<u32>,
    rsr_ecr: ReadWrite<u32>,
    _reserved0: [u8; 0x10],
    fr: ReadOnly<u32, Flags::Register>,
    _reserved1: [u8; 0x8],
    ibrd: ReadWrite<u32, IntDivisor::Register>,
    fbrd: ReadWrite<u32, FracDivisor::Register>,
    lcrh: ReadWrite<u32, LineControl::Register>,
    ctl: ReadWrite<u32, Control::Register>,
    ifls: ReadWrite<u32, FIFOLevelSelect::Register>,
    imsc: ReadWrite<u32, Interrupts::Register>,
    ris: ReadOnly<u32, Interrupts::Register>,
    mis: ReadOnly<u32, Interrupts::Register>,
    icr: WriteOnly<u32, Interrupts::Register>,
    dmactl: ReadWrite<u32>,
}

register_bitfields![
    u32,
    Control [
        UART_ENABLE OFFSET(0) NUMBITS(1) [],
        TX_ENABLE OFFSET(8) NUMBITS(1) [],
        RX_ENABLE OFFSET(9) NUMBITS(1) []
    ],
    LineControl [
        FIFO_ENABLE OFFSET(4) NUMBITS(1) [],
        WORD_LENGTH OFFSET(5) NUMBITS(2) [
            Len5 = 0x0,
            Len6 = 0x1,
            Len7 = 0x2,
            Len8 = 0x3
        ]
    ],
    FIFOLevelSelect [
        TX_LEVEL_SELECT OFFSET(0) NUMBITS(3) [
            Lev1_8 = 0x0,
            Lev1_4 = 0x1,
            Lev1_2 = 0x2,
            Lev3_4 = 0x3,
            Lev7_8 = 0x4
        ],
        RX_LEVEL_SELECT OFFSET(3) NUMBITS(3) [
            Lev1_8 = 0x0,
            Lev1_4 = 0x1,
            Lev1_2 = 0x2,
            Lev3_4 = 0x3,
            Lev7_8 = 0x4
        ]
    ],
    ErrorClear [
        FRAMING_ERROR OFFSET(0)  NUMBITS(1),
        BREAK_ERROR   OFFSET(1)  NUMBITS(1),
        PARITY_ERROR  OFFSET(2)  NUMBITS(1),
        OVERRUN_ERROR OFFSET(3)  NUMBITS(1)
    ],
    IntDivisor [
        DIVISOR OFFSET(0) NUMBITS(16) []
    ],
    FracDivisor [
        DIVISOR OFFSET(0) NUMBITS(6) []
    ],
    Flags [
        TX_FIFO_EMPTY OFFSET(7) NUMBITS(1),
        RX_FIFO_FULL  OFFSET(6) NUMBITS(1),
        TX_FIFO_FULL  OFFSET(5) NUMBITS(1),
        RX_FIFO_EMPTY OFFSET(4) NUMBITS(1),
        BUSY          OFFSET(3) NUMBITS(1),
        CTS           OFFSET(0) NUMBITS(1)
    ],
    Interrupts [
        ALL_INTERRUPTS  OFFSET(0)  NUMBITS(12),
        CTS             OFFSET(1)  NUMBITS(1),
        RX_FIFO_THRESH  OFFSET(4)  NUMBITS(1),
        TX_FIFO_THRESH  OFFSET(5)  NUMBITS(1),
        RX_TIMEOUT      OFFSET(6)  NUMBITS(1),
        FRAMING_ERROR   OFFSET(7)  NUMBITS(1),
        PARITY_ERROR    OFFSET(8)  NUMBITS(1),
        BREAK_ERROR     OFFSET(9)  NUMBITS(1),
        OVERRUN_ERROR   OFFSET(10) NUMBITS(1)
    ],
    Data [
        DATA OFFSET(0) NUMBITS(8)
    ]
];

pub struct UART {
    regs: *const Registers,
    client: Cell<Option<&'static kernel::hil::uart::Client>>,
    tx_buffer: kernel::common::take_cell::TakeCell<'static, [u8]>,
    tx_remaining_bytes: Cell<usize>,
    rx_buffer: kernel::common::take_cell::TakeCell<'static, [u8]>,
    rx_remaining_bytes: Cell<usize>,
    tx_pin: Cell<Option<u8>>,
    rx_pin: Cell<Option<u8>>,
}

pub static mut UART0: UART = UART::new();

impl UART {
    pub const fn new() -> UART {
        UART {
            regs: UART_BASE as *mut Registers,
            client: Cell::new(None),
            tx_buffer: kernel::common::take_cell::TakeCell::empty(),
            tx_remaining_bytes: Cell::new(0),
            rx_buffer: kernel::common::take_cell::TakeCell::empty(),
            rx_remaining_bytes: Cell::new(0),
            tx_pin: Cell::new(None),
            rx_pin: Cell::new(None),
        }
    }

    pub fn set_pins(&self, tx_pin: u8, rx_pin: u8) {
        self.tx_pin.set(Some(tx_pin));
        self.rx_pin.set(Some(rx_pin));
    }

    pub fn configure(&self, params: kernel::hil::uart::UARTParams) {
        let tx_pin = match self.tx_pin.get() {
            Some(pin) => pin,
            None => panic!("Tx pin not configured for UART")
        };

        let rx_pin = match self.rx_pin.get() {
            Some(pin) => pin,
            None => panic!("Rx pin not configured for UART")
        };

        unsafe {
            /*
            * Make sure the TX pin is output/high before assigning it to UART control
            * to avoid falling edge glitches
            */
            gpio::PORT[tx_pin as usize].make_output();
            gpio::PORT[tx_pin as usize].set();

            // Map UART signals to IO pin
            ioc::IOCFG[tx_pin as usize].enable_uart_tx();
            ioc::IOCFG[rx_pin as usize].enable_uart_rx();
        }

        // Disable the UART before configuring
        self.disable();

        self.set_baud_rate(params.baud_rate);

        // Set word length
        let regs = unsafe { &*self.regs };
        regs.lcrh.write(LineControl::WORD_LENGTH::Len8);

        self.fifo_enable();

        // Enable UART, RX and TX
        regs.ctl.write(Control::UART_ENABLE::SET
            + Control::RX_ENABLE::SET
            + Control::TX_ENABLE::SET
        );
    }

    fn power_and_clock(&self) {
        prcm::Power::enable_domain(prcm::PowerDomain::Serial);
        while !prcm::Power::is_enabled(prcm::PowerDomain::Serial) { };
        prcm::Clock::enable_uart_run();
    }

    fn set_baud_rate(&self, baud_rate: u32) {
        // Fractional baud rate divider
        let div = (((MCU_CLOCK * 8) / baud_rate) + 1) / 2;
        // Set the baud rate
        let regs = unsafe { &*self.regs };
        regs.ibrd.write(IntDivisor::DIVISOR.val(div / 64));
        regs.fbrd.write(FracDivisor::DIVISOR.val(div % 64));
    }

    fn fifo_enable(&self) {
        let regs = unsafe { &*self.regs };
        regs.lcrh.modify(LineControl::FIFO_ENABLE::SET);
    }

    fn fifo_disable(&self) {
        let regs = unsafe { &*self.regs };
        regs.lcrh.modify(LineControl::FIFO_ENABLE::CLEAR);
    }

    #[allow(unused)]
    fn enable_rx_interrupts(&self) {
        let regs = unsafe { &*self.regs };
        regs.imsc.modify(Interrupts::RX_FIFO_THRESH::SET);
    }

    fn enable_tx_interrupts(&self) {
        let regs = unsafe { &*self.regs };
        regs.imsc.modify(Interrupts::TX_FIFO_THRESH::SET);
    }

    #[allow(unused)]
    fn disable_rx_interrupts(&self) {
        let regs = unsafe { &*self.regs };
        regs.imsc.modify(Interrupts::RX_FIFO_THRESH::CLEAR);
    }

    fn disable_tx_interrupts(&self) {
        let regs = unsafe { &*self.regs };
        regs.imsc.modify(Interrupts::TX_FIFO_THRESH::CLEAR);
    }

    pub fn disable(&self) {
        self.fifo_disable();
        let regs = unsafe { &*self.regs };
        regs.ctl.modify(Control::UART_ENABLE::CLEAR
            + Control::TX_ENABLE::CLEAR
            + Control::RX_ENABLE::CLEAR);
    }

    pub fn disable_interrupts(&self) {
        // Disable all UART interrupts
        let regs = unsafe { &*self.regs };
        regs.imsc.modify(Interrupts::ALL_INTERRUPTS::CLEAR);
        // Clear all UART interrupts
        regs.icr.write(Interrupts::ALL_INTERRUPTS::SET);
    }

    pub fn handle_interrupt(&self) {
        let regs = unsafe { &*self.regs };
        //test if sending a byte when handler is called works.
        if self.tx_fifo_threshold(){
            //disable interrupts
            self.disable_tx_interrupts();
            //clear the interrupt
            //regs.icr.write(Interrupts::TX_FIFO_THRESH::SET);
            //get remaining bytes in UART buffer
            let mut rem = self.tx_remaining_bytes.get();

            self.tx_buffer.take().map(|tx_buffer|{
                if rem > 0 && self.tx_ready(){
                    regs.dr.set(tx_buffer[self.tx_remaining_bytes.get()-rem] as u32);
                    rem = rem-1;
                } 
            });

            self.tx_remaining_bytes.set(rem);

            if self.tx_remaining_bytes.get() == 0 {
                // Signal client write done
                self.client.get().map(|client| {
                    self.tx_buffer.take().map(|tx_buffer| {
                        client.transmit_complete(
                            tx_buffer,
                            kernel::hil::uart::Error::CommandComplete,
                        );
                    });
                });
            }
            // Not all bytes have been transmitted 
            else {
                self.enable_tx_interrupts();
            }

        }
        // Get status bits
        #[allow(unused)]
        let flags: u32 = regs.fr.get();
        // Clear interrupts
        //regs.icr.write(Interrupts::ALL_INTERRUPTS::SET);
    }

    #[allow(unused)]
    pub fn send_byte(&self, c: u8) {
        // Wait for space in FIFO
        while !self.tx_ready() {}
        // Put byte in data register
        let regs = unsafe { &*self.regs };
        regs.dr.set(c as u32);
    }

    #[allow(unused)]
    pub fn send_buffer(&self) {
        // Wait for space in FIFO
        while !self.tx_ready() {}

        let mut rem = self.tx_remaining_bytes.get();
        // Put byte in data register
        let regs = unsafe { &*self.regs };
        self.tx_buffer.take().map(|tx_buffer|{
            while rem > 0 && self.tx_ready(){
                regs.dr.set(tx_buffer[self.tx_remaining_bytes.get()-rem] as u32);
                rem = rem-1;
            } 
        });
    }

    pub fn tx_fifo_threshold(&self) -> bool {
        let regs = unsafe { &*self.regs };
        regs.mis.is_set(Interrupts::TX_FIFO_THRESH)
    }

    pub fn tx_ready(&self) -> bool {
        let regs = unsafe { &*self.regs };
        !regs.fr.is_set(Flags::TX_FIFO_FULL)
    }
}

impl kernel::hil::uart::UART for UART {
    fn set_client(&self, client: &'static kernel::hil::uart::Client) {
        self.client.set(Some(client));
    }

    fn init(&self, params: kernel::hil::uart::UARTParams) {
        self.power_and_clock();
        self.disable_interrupts();
        self.configure(params);
    }

    fn transmit(&self, tx_data: &'static mut [u8], tx_len: usize) {
        let regs = unsafe { &*self.regs };

        if tx_len == 0 { return; }

        self.tx_remaining_bytes.set(tx_len);
        self.tx_buffer.replace(tx_data);

        regs.ifls.write(FIFOLevelSelect::TX_LEVEL_SELECT::Lev1_2);

        self.enable_tx_interrupts();
    }

    #[allow(unused)]
    fn receive(&self, rx_buffer: &'static mut [u8], rx_len: usize) {}
}